from ops.random import multi_random_choice
from itertools import combinations
import numpy as np


def generate_spatial_triplets(data_df):
    target_groups = data_df[data_df.label > -1].groupby(['page', 'label'])

    triplets = np.zeros((0, 3))
    for (page, label), target_group in target_groups:
        page_nodes = data_df[data_df.page == page]
        negative_candidates = page_nodes[page_nodes.label != label]

        positive_pairs = np.array(list(combinations(target_group.index, 2)))

        if not len(positive_pairs):
            continue

        anchor_centroids = data_df.loc[positive_pairs[:, 0], ['xc', 'yc']].values
        negative_centroids = negative_candidates[['xc', 'yc']].values
        distances = np.linalg.norm(anchor_centroids[:, None, :] - negative_centroids, axis=2)
        dist_weights = np.exp(5 / (distances + 1))
        selection_probabilities = dist_weights / dist_weights.sum(axis=1, keepdims=True)

        negatives = multi_random_choice(negative_candidates.index.values, selection_probabilities)

        triplets = np.vstack([triplets, np.hstack([positive_pairs, negatives.reshape(-1, 1)])])
    return triplets


def generate_spatial_triplets_shuffled_pages(data_df):
    pages = list(data_df.groupby('page'))
    np.random.shuffle(pages)

    triplets = np.zeros((0, 3))
    for page, page_df in pages:
        target_groups = page_df[page_df.label > -1].groupby('label')

        for label, target_group in target_groups:
            positive_pairs = np.array(list(combinations(target_group.index, 2)))
            if not len(positive_pairs):
                continue

            negative_candidates = page_df[page_df.label != label]

            anchor_centroids = data_df.loc[positive_pairs[:, 0], ['xc', 'yc']].values
            negative_centroids = negative_candidates[['xc', 'yc']].values
            distances = np.linalg.norm(anchor_centroids[:, None, :] - negative_centroids, axis=2)
            dist_weights = np.exp(5 / (distances + 1))
            selection_probs = dist_weights / dist_weights.sum(axis=1, keepdims=True)

            negatives = multi_random_choice(negative_candidates.index.values, selection_probs)

            triplets = np.vstack([triplets, np.hstack([positive_pairs, negatives.reshape(-1, 1)])])
    return triplets


def triplet_mining_nearest_negative(data_df, data_list, embedding_model):
    embedding_model.eval()

    # generate embeddings for every page
    embeddings = {}
    for data_ind in data_df.page.unique():
        graph = data_list[data_ind]
        embeddings[data_ind] = embedding_model(
            graph.x_dict,
            graph.edge_index_dict,
        ).detach().numpy()

    # shuffle pages
    pages = data_df.page.unique()
    np.random.shuffle(pages)

    triplets = np.zeros((0, 3))
    for page in pages:
        page_df = data_df[data_df.page == page]
        page_embeddings = embeddings[page]
        target_groups = page_df[page_df.label > -1].groupby('label')

        # for each positive pair, match to anchor's closest negative node
        for label, target_group in target_groups:

            # positives
            positive_pairs = np.array(list(combinations(target_group.index, 2)))
            if not len(positive_pairs):
                continue

            positive_inds = target_group.index.values
            positive_nodes = page_df.loc[positive_inds].node
            positive_embeddings = page_embeddings[positive_nodes]
            positive_match_inds = np.argwhere(positive_pairs[:, 0] == positive_inds[:, None])[:, 0]

            negatives = page_df[page_df.label != label].index.values
            negative_inds = np.where(page_df.label != label)[0]
            negative_embeddings = page_embeddings[negative_inds]

            distances = np.linalg.norm(positive_embeddings[:, None, :] - negative_embeddings, axis=2)
            negative_matches = negatives[distances.argmin(axis=1)][positive_match_inds]

            # todo: is list.append() -> np.array faster for full loop?
            triplets = np.vstack([triplets, np.hstack([positive_pairs, negative_matches.reshape(-1, 1)])])

    return triplets


def generate_triplets_naive(data_df, max_triplets_per_label=10):
    target_groups = data_df[data_df.label > -1].groupby(['page', 'label'])
    picked_inds = np.zeros(len(data_df)).astype(bool)

    # shuffle target groups
    target_groups = list(target_groups)
    np.random.shuffle(target_groups)  # Removing this'll speed up training even more

    triplets = np.zeros((0, 3))
    for (page, label), target_group in target_groups:

        # remove already picked indices
        target_group = target_group[~picked_inds[target_group.index]]

        # generate positives
        n_nodes = len(target_group)
        n_pairs = min(n_nodes // 2, max_triplets_per_label)

        shuffled_group = target_group.sample(frac=1).index
        positive_pairs = shuffled_group.values[:n_pairs * 2].reshape(-1, 2)

        if not len(positive_pairs):
            continue

        # generate negatives
        negative_candidates = data_df[np.logical_and(data_df.page == page, ~picked_inds)].drop(target_group.index,
                                                                                               axis=0, errors='ignore')

        if len(negative_candidates) < len(positive_pairs):
            continue

        negative_indices = negative_candidates.sample(len(positive_pairs)).index
        picked_inds[negative_indices] = 1
        picked_inds[positive_pairs.flatten()] = 1

        negative_indices = negative_indices.values.reshape(-1, 1)

        triplets = np.vstack([triplets, np.hstack([positive_pairs, negative_indices])])
        #np.random.shuffle(triplets)

    return triplets
