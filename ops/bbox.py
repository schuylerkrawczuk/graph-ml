import numpy as np


def batch_iou(boxes_1: np.ndarray, boxes_2: np.ndarray):
    """
    intersection over union between each box of two box sets
        Args:
            boxes_1 (np.array[N, 4]): set of N boxes (x0, y0, x1, y1)
            boxes_2 (np.array[M, 4]): second set of M boxes

        Returns:
            np.array[N, M]: pair-wise IoU between each element of boxes_1 and boxes_2
    """
    area_1 = box_area(boxes_1)
    area_2 = box_area(boxes_2)

    inter_top_left = np.maximum(boxes_1[:, None, :2], boxes_2[:, :2])
    inter_bottom_right = np.minimum(boxes_1[:, None, 2:], boxes_2[:, 2:])
    inter_width_height = np.clip(inter_bottom_right - inter_top_left, a_min=0, a_max=np.inf)

    intersection = inter_width_height[:, :, 0] * inter_width_height[:, :, 1]
    union = area_1[:, None] + area_2 - intersection

    return intersection / union


def batch_overlap(boxes_1: np.ndarray, boxes_2: np.ndarray, dim):
    """
    percent intersection of two box sets in a given dimension
        Args:
            boxes_1 (np.array[N, 4]): set of N boxes (x0, y0, x1, y1)
            boxes_2 (np.array[M, 4]): second set of M boxes
            dim (int): dimension in which to calculate overlap. 0 -> x, 1 -> y

        Returns:
            np.array[N, M]: pair-wise percent intersection of boxes_1 with boxes_2
    """
    length_1 = boxes_1[:, dim + 2] - boxes_1[:, dim]

    inter_top = np.maximum(boxes_1[:, None, dim], boxes_2[:, dim])
    inter_bottom = np.minimum(boxes_1[:, None, dim + 2], boxes_2[:, dim + 2])
    intersection = np.clip(inter_bottom - inter_top, a_min=0, a_max=np.inf)
    percent_overlap = intersection / length_1

    return percent_overlap


def box_area(boxes: np.ndarray):
    """
    returns areas of each box in set of boxes
    """
    width_height = boxes[:, 2:] - boxes[:, :2]
    areas = width_height[:, 0] * width_height[:, 1]
    return areas
