import torch


class TripletLoss(torch.nn.Module):
    def __init__(self, **kwargs):
        super().__init__()
        self.triplet_loss = torch.nn.TripletMarginLoss(**kwargs)
        self.margin = self.triplet_loss.margin

    def forward(self, anchor, positive, negative):
        triplet_loss = self.triplet_loss(anchor, positive, negative)
        return triplet_loss, self.triplet_stats(triplet_loss)

    def triplet_stats(self, loss):
        loss = loss.detach()
        n_hard = len(self.get_hard_negative(loss))
        n_semihard = len(self.get_semi_hard_negative(loss))
        n_pos = len(loss) - n_hard - n_semihard
        return {
            'positive': n_pos,
            'hard_negative': n_hard,
            'semi_hard_negative': n_semihard,
        }

    def get_semi_hard_negative(self, loss):
        semi_hard_mask = torch.logical_and(loss < self.margin, loss > 0.)
        return loss[semi_hard_mask]

    def get_hard_negative(self, loss):
        hard_mask = loss > self.margin
        return loss[hard_mask]


class SemiHardTripletLoss(TripletLoss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def forward(self, anchor, positive, negative):
        triplet_loss = self.triplet_loss(anchor, positive, negative)
        semi_hard_loss = self.get_semi_hard_negative(triplet_loss)
        return semi_hard_loss, self.triplet_stats(triplet_loss)


class HardTripletLoss(TripletLoss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def forward(self, anchor, positive, negative):
        triplet_loss = self.triplet_loss(anchor, positive, negative)
        hard_loss = self.get_hard_negative(triplet_loss)
        return hard_loss, self.triplet_stats(triplet_loss)


class NegativeTripletLoss(TripletLoss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def forward(self, anchor, positive, negative):
        triplet_loss = self.triplet_loss(anchor, positive, negative)
        negative_loss = triplet_loss[triplet_loss > 0.]
        return negative_loss, self.triplet_stats(triplet_loss)
