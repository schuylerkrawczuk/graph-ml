import numpy as np


def multi_random_choice(a: np.ndarray, p: np.ndarray):
    """
    Vectorized implementation of weighted np.random.choice.
        Args:
            a (np.array[m]): array from where random samples are generated
            p (np.array[n, m]): probabilities of selection.

        Returns:
             np.array[n]: random selections from a
    """
    prob_cumsum = np.cumsum(p, axis=1)
    selection_inds = (np.random.random((p.shape[0], 1)) > prob_cumsum).sum(axis=1)
    return a[selection_inds]
