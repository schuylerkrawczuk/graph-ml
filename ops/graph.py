from collections import defaultdict
from torch_geometric.data import HeteroData
from scipy.sparse.csgraph import connected_components
import numpy as np


def graph_from_df(graph_df):
    graph_dict = defaultdict(list)
    for node in np.unique(graph_df[['source', 'target']].values.flatten()):
        graph_dict[node].extend(graph_df[graph_df.source == node].target.tolist())
        graph_dict[node].extend(graph_df[graph_df.target == node].source.tolist())
    return graph_dict


def neighbor_depth(neighbors, graph: dict, node: int, depth=0, max_depth=np.inf):
    """
    all nodes within a degree of separation from source node
    """
    neighbors[node] = min(neighbors[node], depth)
    if depth < max_depth:
        for neighbor in graph[node]:
            neighbor_depth(neighbors, graph, neighbor, depth + 1, max_depth)


def connected_neighbors(graph, node, max_depth):
    """
    get nodes connected by a maximum depth to source node
    """
    neighbors = defaultdict(lambda: np.inf)
    neighbor_depth(neighbors, graph, node=node, depth=0, max_depth=max_depth)
    return list(set(neighbors.keys()) - {node})


def is_connected(node_index_1, node_index_2, graph):
    """
    check if two nodes are directly connected
    """
    return node_index_1 in graph[node_index_2] or node_index_2 in graph[node_index_1]


def separate_connected_components(graph: HeteroData, nodes: list):
    """
    divide nodes into groups of directly connected components
    """
    adjacency = np.zeros((graph.size()))
    adjacency[tuple(graph.to_homogeneous().edge_index.numpy())] = 1

    _, connected_component_labels = connected_components(
        adjacency[tuple(np.meshgrid(nodes, nodes))], directed=True)

    return connected_component_labels
