import torch


def intersection(a: torch.Tensor, b: torch.Tensor):
    """
    Get mutual values of two arbitrarily sized tensors
        Args:
            a (torch.tensor):
            b (torch.tensor):

        Returns:
            torch.tensor: values that are in both a and b
    """
    a_cat_b, counts = torch.cat([a.unique(), b.unique()]).unique(return_counts=True)
    return a_cat_b[torch.where(counts.gt(1))]
