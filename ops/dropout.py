from typing import Union, List, Dict, Optional
from torch import nn
import torch


class IndexDropout(nn.Module):
    def __init__(self, input_size: int, p: float, index: Union[int, List[int], torch.Tensor],
                 feature_category: Optional[str] = None):
        """
        Apply dropout to specific dimensions of a tensor
        Args:
            input_size: size of feature dimension
            p: dropout probability
            index: feature indices to apply dropout to
            feature_category: name of feature category being applied to (optional)
        """
        super().__init__()
        self.prob = p
        self.dropout_indices = torch.tensor(index)
        self.input_size = input_size
        self.register_buffer('dropout_mask', ~torch.isin(torch.arange(input_size), self.dropout_indices))
        self.feature_category = feature_category

    def forward(self, x: Dict[str, torch.Tensor]):
        if self.training:
            if torch.rand(1) < self.prob:
                for node_type in x:
                    x[node_type] *= self.dropout_mask

        return x
