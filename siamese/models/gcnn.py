from typing import Dict, Tuple
from collections import defaultdict
from time import time
from siamese.models.mutlilingual import EmbeddingReduction
from ops.dropout import IndexDropout
from torch_geometric.nn import GATConv, Linear, GraphConv, SAGEConv
from torch import nn, Tensor
import torch
import pandas as pd


class GCNN(nn.Module):
    def __init__(self, hidden_channels, out_channels):
        super().__init__()
        self.conv1 = GraphConv((-1, -1), hidden_channels)
        self.conv2 = GraphConv((-1, -1), hidden_channels)
        self.conv3 = GraphConv((-1, -1), hidden_channels)
        self.conv4 = GraphConv((-1, -1), out_channels)

    def forward(self, x, edge_index):
        x1 = self.conv1(x, edge_index).relu()
        x2 = (self.conv2(x1, edge_index) + x1).relu()
        x3 = (self.conv3(x2, edge_index) + x2).relu()
        x4 = (self.conv4(x3, edge_index) + x3).relu()
        x = nn.functional.normalize(x4, p=2)
        return x


class GCNNModel(nn.Module):
    """
    Wrapper for GCNN in order to keep consistency between models w.r.t. heterogeneity
    """
    def __init__(self, hidden_channels, out_channels, text_embedding_size, feature_categories):
        super().__init__()
        self.gcnn = GCNN(hidden_channels, out_channels)
        self.feature_categories = feature_categories
        self.dropouts = self.init_dropout()

    def forward(self, x, edge_index):
        for dropout in self.dropouts:
            x = dropout(x)
        return self.gcnn(x, edge_index)['node']

    def init_dropout(self):
        dropouts = []
        for feature_type in self.feature_categories.feature_type.unique():
            type_features = self.feature_categories[self.feature_categories.feature_type == feature_type]
            dropout_prob = type_features.dropout.iloc[0]
            if len(type_features) and dropout_prob > 0:
                dropouts.append(IndexDropout(input_size=len(self.feature_categories),
                                             p=dropout_prob,
                                             index=type_features.index,
                                             feature_category=feature_type
                                             ))

        return dropouts


class GCNNMultilingual(nn.Module):
    """
        Includes embedding reduction for multilingual embedding before GCNN
    """
    def __init__(self, hidden_channels, out_channels, text_embedding_size, feature_categories):
        super().__init__()
        self.embedding_reduction = EmbeddingReduction(input_size=512, output_size=text_embedding_size)
        self.gcnn = GCNN(hidden_channels, out_channels)
        self.feature_categories = self.update_feature_categories(feature_categories)

    def forward(self, x, edge_index):
        x = self.embedding_reduction(x)
        x = self.gcnn(x, edge_index)
        return x

    def update_feature_categories(self, feature_categories):
        feat_size = self.embedding_reduction.fc1.in_features
        new_features = pd.DataFrame({
            'col_name': [f'text_embed_{i}' for i in range(feat_size)],
            'feature_type': ['text'] * feat_size
        })
        feature_categories = feature_categories.append(new_features, ignore_index=True)
        return feature_categories


class GCNNMultilingualDropout(nn.Module):
    """
        Includes embedding reduction for multilingual embedding before GCNN
        Applies Dropout per- feature type after embedding using feature_categories
    """
    def __init__(self, hidden_channels, out_channels, text_embedding_size, feature_categories):
        super().__init__()
        self.embedding_reduction = EmbeddingReduction(input_size=512, output_size=text_embedding_size)
        self.gcnn = GCNN(hidden_channels, out_channels)
        self.feature_categories = self.update_text_feature_categories(feature_categories)
        self.dropouts = self.init_dropout()

    def forward(self, x, edge_index):
        x = self.embedding_reduction(x)
        for dropout in self.dropouts:
            x = dropout(x)
        x = self.gcnn(x, edge_index)['node']
        return x

    def update_text_feature_categories(self, feature_categories):
        """
            Updates feature categories for text embedding
        """
        text_inds = feature_categories.feature_type == 'text'
        text_dropout = feature_categories.loc[text_inds, 'dropout'].iloc[0]
        feat_size = self.embedding_reduction.fc4.out_features
        new_features = pd.DataFrame({
            'col_name': [f'text_embed_{i}' for i in range(feat_size)],
            'feature_type': ['text'] * feat_size,
            'dropout': [text_dropout] * feat_size
        })
        feature_categories = feature_categories[~text_inds].append(new_features, ignore_index=True)
        return feature_categories

    def init_dropout(self):
        dropouts = []
        for feature_type in self.feature_categories.feature_type.unique():
            type_features = self.feature_categories[self.feature_categories.feature_type == feature_type]
            dropout_prob = type_features.dropout.iloc[0]
            if len(type_features) and dropout_prob > 0:
                dropouts.append(IndexDropout(input_size=len(self.feature_categories),
                                             p=dropout_prob,
                                             index=type_features.index,
                                             feature_category=feature_type
                                             ))

        return dropouts


class GCNNWithClassifier(nn.Module):
    """
        Generates embedding and classifies nodes
    """
    def __init__(self, hidden_channels, out_channels, text_embedding_size, feature_categories):
        super().__init__()
        self.embedding_model = GCNNMultilingualDropout(hidden_channels, out_channels, text_embedding_size,
                                                       feature_categories)
        self.node_classifier = NodeClassifier(hidden_channels)
        self.feature_categories = self.embedding_model.feature_categories

    def forward(self, x, edge_index):
        x = self.embedding_model.embedding_reduction(x)
        embeddings = self.embedding_model(x, edge_index)
        classifications = self.node_classifier(embeddings)
        return embeddings['node'], classifications['node']


class NodeClassifier(nn.Module):
    def __init__(self, hidden_size):
        super().__init__()
        self.lin1 = nn.Linear(32, hidden_size)
        self.lin2 = nn.Linear(hidden_size, 1)

    def forward(self, x):
        x = self.lin1(x).relu()
        x = torch.sigmoid(self.lin2(x))
        return x
