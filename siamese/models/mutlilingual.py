from typing import Dict
from collections import defaultdict
from time import time
from tensorflow.keras.models import load_model
from torch import nn, fx, Tensor
import torch
import tensorflow_text
from sys import getsizeof

fx.wrap('get_tf_output')


def get_tf_output(text_list, model):
    return torch.tensor(model(text_list).numpy())


class MultilingualEmbedding:
    def __init__(self, model_path):
        self.ml_model = load_model(model_path)  # TODO: load to device

    def forward(self, x, text_list):
        embedding = get_tf_output(text_list, self.ml_model)
        return torch.hstack([x, embedding])


class EmbeddingReduction(nn.Module):
    def __init__(self, input_size, output_size):
        super().__init__()
        self.input_size = input_size
        self.fc1 = nn.Linear(input_size, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, 32)
        self.fc4 = nn.Linear(32, output_size)
        self.dropout = nn.Dropout(p=0.2)

    def forward(self, x):
        x1 = x['node'][:, -self.input_size:]
        x1 = self.fc1(x1).relu()
        x1 = self.dropout(x1)
        x1 = self.fc2(x1).relu()
        x1 = self.dropout(x1)
        x1 = self.fc3(x1).relu()
        x1 = self.dropout(x1)
        x1 = self.fc4(x1).relu()
        x['node'] = x['node'][:, :-self.input_size]
        x['node'] = torch.hstack([x['node'], x1])
        return x

