from torch_geometric.nn import global_sort_pool
from torch import nn
import torch


class ClusterClassifier(nn.Module):


    def __init__(self, embedding_size, k):
        """
            Args:
                embedding_size (int): size of input embeddings
                k (int): number of nodes to keep after pooling
        """
        super().__init__()
