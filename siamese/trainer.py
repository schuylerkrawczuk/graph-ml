from sklearn.svm import SVC
from sklearn.cluster import DBSCAN
from ops.triplet_generation import * #triplet_mining_centroid, triplet_mining_nearest_negative, generate_triplets_naive
import torch
import os
import time
import pandas as pd
import numpy as np
import pickle


class SiameseTrainer:
    def __init__(self, input_dict, params):
        self.max_triplets_per_label = params['max_triplets_per_label']
        self.model = input_dict['model']
        self.optimizer = input_dict['optimizer']
        self.loss_functions = input_dict['loss_function']
        self.loss_function = None
        self.data_loaders = input_dict['data_loaders']
        self.params = params
        self.epoch = 0
        self.batch_sizes = params['batch_size']
        self.batch_size = None
        self.params['phases'] = [p for p in self.data_loaders]
        self.data_df = {p: self.create_data_df(self.data_loaders[p]) for p in self.params['phases']}
        self.log = {p: pd.DataFrame() for p in self.params['phases']}
        self.triplet_log = {p: pd.DataFrame() for p in self.params['phases']}
        self.last_checkpoint = None
        self.init_model()

    def init_model(self):
        if not os.path.isdir(self.params['model_dir']):
            os.mkdir(self.params['model_dir'])

        if 'checkpoint' in self.params['model'] and self.params['model']['checkpoint'] is not None:
            checkpoint = torch.load(os.path.join(self.params['model_dir'], self.params['model']['checkpoint']),
                                    map_location=self.params['device'])
            model_state_dict = checkpoint['model_state_dict']
            self.epoch = checkpoint['epoch'] + 1
            self.model.load_state_dict(model_state_dict)
            self.model.to(self.params['device'])
            batch_key = min([e for e in self.loss_functions if e <= self.epoch])
            self.loss_function = self.loss_functions[batch_key]
            batch_key = min([e for e in self.batch_sizes if e <= self.epoch])
            self.batch_size = self.batch_sizes[batch_key]
            self.last_checkpoint = self.params['model']['checkpoint']

            print(f'resuming from {self.params["model"]["checkpoint"]} at epoch {self.epoch}')
            for phase in self.params['phases']:
                log = pd.read_csv(os.path.join(self.params['model_dir'], f'{phase}_log.csv'), index_col=0)
                triplet_log = pd.read_csv((os.path.join(self.params['model_dir'], f'{phase}_triplets.csv')), index_col=0)
                if len(log) == self.epoch: # todo: why's this not working?
                    self.log[phase] = log
                    self.triplet_log[phase] = triplet_log

    def train(self, n_epochs):
        self.model.to(self.params['device'])
#        print(f'{"Dropout": >20}')
#        for c in self.model.feature_categories.feature_type.unique():
#            print(f'{c: >20}: {self.model.feature_categories[self.model.feature_categories.feature_type == c].dropout.iloc[0]}')

        for epoch in range(self.epoch, n_epochs):
            if epoch in self.loss_functions:
                self.loss_function = self.loss_functions[epoch]
            if epoch in self.batch_sizes:
                self.batch_size = self.batch_sizes[epoch]

            triplet_stats = pd.DataFrame()
            for phase in self.params['phases']:
                print(40 * '-' + f' {phase} ' + 40 * '-')

                if phase == 'train':
                    self.model.train()
                else:
                    self.model.eval()

                phase_loss = {'triplet_loss': []}

                triplets = self.generate_triplets(self.data_df[phase], self.data_loaders[phase])
                batches = [triplets[i: i + self.batch_size]
                           for i in range(0, len(triplets), self.batch_size)]

                for i, batch in enumerate(batches):
                    t_start = time.time()
                    pages = self.data_df[phase].iloc[batch.flatten()].page
                    node_inds = self.data_df[phase].iloc[batch.flatten()].node

                    embeddings = {}
                    for data_ind in pages.unique():
                        graph = self.data_loaders[phase][data_ind]
                        embeddings[data_ind] = self.model(
                            graph.x_dict,
                            graph.edge_index_dict,
                        )

                    triplet_embeddings = []
                    for page, node in zip(pages, node_inds):
                        triplet_embeddings.append(embeddings[page][node])

                    triplet_embeddings = torch.vstack(triplet_embeddings)
                    triplet_embeddings = torch.vstack(  # torch.split?
                        [triplet_embeddings[None, i: i + 3] for i in range(0, len(triplet_embeddings), 3)])

                    triplet_loss, stats = self.loss_function(*triplet_embeddings.permute(1, 0, 2))
                    triplet_stats = triplet_stats.append(stats, ignore_index=True)

                    loss = triplet_loss.mean()
                    phase_loss['triplet_loss'].append(triplet_loss.mean().cpu().detach())

                    if phase == 'train':
                        self.optimizer.zero_grad()
                        loss.backward()
                        self.optimizer.step()

                    t_end = time.time()
                    n_triplets = triplet_loss.shape[0]
                    print(f'Epoch {epoch: {5}}|  Iteration: {i: {5}}/{len(batches): {5}}|  Loss: {loss.item(): 5.5f}|  Time: {t_end - t_start: 5.5f}|, Triplets: {n_triplets: 2.0f}')

                self.triplet_log[phase] = self.triplet_log[phase].append(triplet_stats.sum(), ignore_index=True)
                self.log[phase] = self.log[phase].append(pd.DataFrame(phase_loss).mean(), ignore_index=True)
            self.save_checkpoint(epoch, f'epoch_{epoch+1}.pth')

    def generate_triplets(self, data_df, data_list):
        t1 = time.time()
        triplets = triplet_mining_nearest_negative(data_df, data_list, self.model)
        #triplets = generate_triplets_naive(data_df, self.max_triplets_per_label)
        print('triplet mining:', time.time() - t1)
        return triplets

    @staticmethod
    def create_data_df(data_list):
        data_df = pd.DataFrame()
        for i, graph in enumerate(data_list):
            labels = graph.y_dict['node'].numpy()
            bboxes = pd.DataFrame(graph.bbox_dict['node'].numpy(),
                                  columns=['x0', 'y0', 'x1', 'y1'])

            graph_df = pd.DataFrame({
                'node': range(len(labels)),
                'label': labels,
                'page': [i] * len(labels)
            })
            graph_df = pd.concat([graph_df, bboxes], axis=1)
            data_df = data_df.append(graph_df, ignore_index=True)
        data_df['xc'] = (data_df.x1 + data_df.x0) / 2
        data_df['yc'] = (data_df.y1 + data_df.y0) / 2

        return data_df

    def save_checkpoint(self, epoch, filename):
        checkpoint_path = os.path.join(self.params['model_dir'], filename)
        save_dict = {
            'epoch': epoch,
            'model_state_dict': self.model.state_dict(),
        }
        torch.save(save_dict, checkpoint_path)

        for phase in self.params['phases']:
            self.log[phase].to_csv(os.path.join(self.params['model_dir'], f'{phase}_log.csv'))
            self.triplet_log[phase].to_csv(os.path.join(self.params['model_dir'], f'{phase}_triplets.csv'))
        self.last_checkpoint = checkpoint_path


class ClusterClassifierTrainer:
    def __init__(self, embedding_model: torch.nn.Module, train_data_list, test_data_list, params):
        self.embedding_model = embedding_model
        self.embedding_model.eval()
        self.clustering_model = DBSCAN(eps=params['margin'], min_samples=2)
        self.cluster_classifier = SVC()
        self.train_data_list = train_data_list
        self.test_data_list = test_data_list
        self.label_tol = 0.5
        self.params = params

    def train(self):
        print('Training cluster classifier')
        mean_clusters_train, cluster_type_train = self.generate_dataset(self.train_data_list)
        mean_clusters_test, cluster_type_test = self.generate_dataset(self.test_data_list)
        self.cluster_classifier.fit(mean_clusters_train, cluster_type_train)

        test_label_pred = self.cluster_classifier.predict(mean_clusters_test)
        accuracy = (test_label_pred == cluster_type_test).mean()
        recall = test_label_pred[np.logical_and(test_label_pred == 1, cluster_type_test == 1)].mean()
        precision = (test_label_pred[test_label_pred == 1] == cluster_type_test[test_label_pred == 1]).mean()
        print(f'% header clusters in training: {cluster_type_train.sum()}')
        print(f'accuracy:  {accuracy :.3}')
        print(f'precision: {precision :.3}')
        print(f'recall:    {recall :.3}')

    def generate_dataset(self, data_list):
        mean_clusters = []
        cluster_type = []
        for graph in data_list:
            embeddings = self.embedding_model(graph.x_dict, graph.edge_index_dict).detach()
            cluster_labels = self.clustering_model.fit_predict(embeddings.detach())
            true_labels = (graph.y_dict['node'] > -1).float()

            for label in np.unique(cluster_labels[cluster_labels > -1]):
                cluster_type.append((true_labels[cluster_labels == label].mean() > self.label_tol).numpy())
                mean_clusters.append(embeddings[cluster_labels == label].mean(dim=0).numpy())

        mean_clusters = np.vstack(mean_clusters)
        cluster_type = np.hstack(cluster_type)
        return mean_clusters, cluster_type

    def save_model(self, checkpoint_path):
        checkpoint = torch.load(os.path.join(self.params['model_dir'], checkpoint_path),
                                map_location=self.params['device'])
        checkpoint['cluster_classifier'] = pickle.dumps(self.cluster_classifier)
        torch.save(checkpoint, os.path.join(self.params['model_dir'], checkpoint_path))
