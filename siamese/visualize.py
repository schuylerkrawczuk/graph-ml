from inference.entity_detector.entity_detector import GraphEntityDetector
from inference.inference_utils import bounding_box, draw_boxes
from siamese.eval import cluster_boxes_from_labels
from siamese import models
from siamese.data import load_data_set, create_data_list
from sklearn.cluster import DBSCAN
from torch_geometric.nn import to_hetero
from tqdm import tqdm
import torch
import os
import yaml
import argparse
import pickle
import cv2


def run_eval(params):
    nodes, edges, bboxes, feature_categories = load_data_set(
        os.path.join(params['data_dir'], params['data_tag']), '', bbox=True, categories=True)
    data_list, file_list, metadata, _ = create_data_list(
        nodes, edges, feature_categories, params['text_embedding_model_path'], '')

    for feat_type, dropout_p in params['dropout'].items():
        feature_categories.loc[feature_categories.feature_type == feat_type, 'dropout'] = dropout_p

    model_class = eval(f"models.{params['model']['model_class']}")
    embedding_model_checkpoint = torch.load(os.path.join(params['model_dir'], params['model']['checkpoint']))
    embedding_model = model_class(params['model']['n_channels'], params['model']['node_embedding_size'],
                                  params['model']['text_embedding_size'], feature_categories)

    embedding_model.gcnn = to_hetero(embedding_model.gcnn, metadata, aggr='sum')
    embedding_model.load_state_dict(embedding_model_checkpoint['model_state_dict'])

    if 'cluster_classifier' in embedding_model_checkpoint:
        print('Using cluster classifier')
        cluster_classifier = pickle.loads(embedding_model_checkpoint['cluster_classifier'])
    else:
        print('No cluster classifier available')
        cluster_classifier = None

    clustering_model = DBSCAN(eps=params['model']['cluster_eps'], min_samples=2)
    header_detector = GraphEntityDetector(embedding_model, clustering_model, cluster_classifier)

    for (graph, file) in tqdm(zip(data_list, file_list)):
        page_bboxes = bboxes[bboxes.file == file]

        pred_labels = header_detector.get_node_labels(graph)

        pred_boxes = cluster_boxes_from_labels(pred_labels, page_bboxes)

        if not os.path.isdir(params['out_dir']):
            os.mkdir(params['out_dir'])

        img = cv2.imread(os.path.join(params['img_dir'], file + '.png'))
        if img is None:
            continue

        img = draw_boxes(img, pred_boxes, (0, 0, 255), 5)
        img = draw_boxes(img, page_bboxes.loc[pred_labels > -1, ['x0', 'y0', 'x1', 'y1']].to_numpy(), (0, 0, 255), 2)
        cv2.imwrite(os.path.join(params['out_dir'], file + '_result.png'), img)


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='visualize model output')
    argc.add_argument('params', type=str, help='evaluation params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    run_eval(params)
