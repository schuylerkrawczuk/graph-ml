"""
    Trains siamese model given the following parameters:
        model_dir: directory to load/save model
        data_dir: main directory for dataset
        data_tag: name of sub-dir for dataset. This includes the nodes, edges, feature categories
        target_name: name of target column in node features df
        n_epochs: number of epochs to train for
        learning_rate: learning rate for training
        max_triplets_per_label: max number of triplets to use from a given object on a page per epoch
        margin: margin used in triplet loss
        batch_size: {starting epoch: batch size} i.e. - 0: 32 \ 50: 64
        loss: {starting epoch: loss function in ops.losses}
        dropout: percent dropout as applied to each feature category:
            text (embedding)
            text_features (non-embedding)
            structural_features
            line_features
        model:
            model_class: class of model. Imported from siamese.models
            hidden_size: number of channels for layers
            node_embedding_size: output node embedding size
            text_embedding_size: size of text embedding reduction output
            checkpoint: path of checkpoint in model dir if not starting from scratch
        train_cluster_classifier: trains a classifier to discern clusters as target/non-target after model trained
        text_embedding_model_path: local path to the multilingual model. Not necessary if embedding already in data
"""

from siamese.data import create_data_list, load_data_set
from siamese.trainer import SiameseTrainer, ClusterClassifierTrainer
from siamese import models
from ops import losses
from torch_geometric.nn import to_hetero
import torch
import os
import yaml
import argparse
torch.manual_seed(13)


def run_training(params):

    train_nodes, train_edges, train_bboxes, feature_categories = load_data_set(
        os.path.join(params['data_dir'], params['data_tag']), 'train', categories=True)
    train_data_list, train_file_list, metadata, feature_categories = create_data_list(
        train_nodes, train_edges, train_bboxes, feature_categories, params['text_embedding_model_path'],
        params['device'], params['target_name'])

    test_nodes, test_edges, test_bboxes, feature_categories = load_data_set(
        os.path.join(params['data_dir'], params['data_tag']), 'test', categories=True)
    test_data_list, test_file_list, _, _ = create_data_list(
        test_nodes, test_edges, test_bboxes, feature_categories, params['text_embedding_model_path'],
        params['device'], params['target_name'])

    for feat_type, dropout_p in params['dropout'].items():
        feature_categories.loc[feature_categories.feature_type == feat_type, 'dropout'] = dropout_p

    model_class = eval(f"models.{params['model']['model_class']}")
    embedding_model = model_class(
        params['model']['hidden_size'],
        params['model']['node_embedding_size'],
        params['model']['text_embedding_size'],
        feature_categories
    )
    embedding_model.gcnn = to_hetero(embedding_model.gcnn, metadata, aggr='sum')

    trainable_params = [p for p in embedding_model.parameters() if p.requires_grad]
    optimizer = torch.optim.Adam(trainable_params, lr=params['learning_rate'], weight_decay=0.0001)

    loss_dict = {
        k: eval(f"losses.{v}(margin=params['margin'], reduction='none')")
        for k, v in params['loss'].items()
    }

    training_pieces = {
        'model': embedding_model,
        'optimizer': optimizer,
        'data_loaders': {
            'train': train_data_list,
            'val': test_data_list
        },
        'loss_function': loss_dict,
    }

    trainer = SiameseTrainer(training_pieces, params)
    trainer.train(params['n_epochs'])

    if params['train_cluster_classifier']:
        cluster_clf_trainer = ClusterClassifierTrainer(
            trainer.model,
            train_data_list,
            test_data_list,
            params
        )
        cluster_clf_trainer.train()
        cluster_clf_trainer.save_model(trainer.last_checkpoint)


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='train model')
    argc.add_argument('params', type=str, help='training params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    run_training(params)
