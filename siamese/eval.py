from inference.entity_detector.entity_detector import GraphEntityDetector
from inference.inference_utils import bounding_box, draw_boxes
from siamese import models
from siamese.data import load_data_set, create_data_list
from sklearn.cluster import DBSCAN
from ops.bbox import batch_iou
from torch_geometric.nn import to_hetero
from tqdm import tqdm
import torch
import os
import numpy as np
import yaml
import argparse
import pickle
import cv2


def cluster_boxes_from_labels(labels, bboxes):
    label_boxes = []
    for label in np.unique(labels[labels > -1]):
        label_boxes.append(bounding_box(bboxes[labels == label]))

    return np.array(label_boxes)


def run_eval(params):
    print(params['model']['checkpoint'])
    nodes, edges, bboxes, feature_categories = load_data_set(
        os.path.join(params['data_dir'], params['data_tag']), 'test', categories=True)
    data_list, file_list, metadata, _ = create_data_list(
        nodes, edges, bboxes, feature_categories, params['text_embedding_model_path'], params['device'], params['target_name'])

    for feat_type, dropout_p in params['dropout'].items():
        feature_categories.loc[feature_categories.feature_type == feat_type, 'dropout'] = dropout_p

    model_class = eval(f"models.{params['model']['model_class']}")
    embedding_model_checkpoint = torch.load(os.path.join(params['model_dir'], params['model']['checkpoint']))
    embedding_model = model_class(params['model']['hidden_size'], params['model']['node_embedding_size'],
                                  params['model']['text_embedding_size'], feature_categories)

    embedding_model.gcnn = to_hetero(embedding_model.gcnn, metadata, aggr='sum')
    embedding_model.load_state_dict(embedding_model_checkpoint['model_state_dict'])

    if 'cluster_classifier' in embedding_model_checkpoint:
        print('Using cluster classifier')
        cluster_classifier = pickle.loads(embedding_model_checkpoint['cluster_classifier'])
    else:
        print('No cluster classifier available')
        cluster_classifier = None

    clustering_model = DBSCAN(eps=params['model']['cluster_eps'], min_samples=2)
    header_detector = GraphEntityDetector(embedding_model, clustering_model, cluster_classifier)

    pred_iou, gt_iou = [], []
    tpos, fpos, fneg = [], [], []

    for (graph, file) in tqdm(zip(data_list, file_list)):
        page_bboxes = bboxes[bboxes.file == file]

        if len(graph.x_dict['node']) != len(page_bboxes):
            print(file)
            continue

        pred_labels = header_detector.get_node_labels(graph)
        gt_labels = graph.y_dict['node'].numpy()
        if not (gt_labels > -1).any():
            continue

        tpos.extend(np.logical_and(pred_labels > -1, gt_labels > -1).tolist())
        fpos.extend(np.logical_and(pred_labels > -1, gt_labels == -1).tolist())
        fneg.extend(np.logical_and(pred_labels == -1, gt_labels > -1).tolist())

        pred_boxes = cluster_boxes_from_labels(pred_labels, page_bboxes)
        gt_boxes = cluster_boxes_from_labels(gt_labels, page_bboxes)

        if len(pred_boxes):
            box_iou = batch_iou(pred_boxes, gt_boxes)
            pred_iou.extend(box_iou.max(axis=1).tolist())
            gt_iou.extend(box_iou.max(axis=0).tolist())
        else:
            gt_iou.extend([0] * len(gt_boxes))

        if params['visualize']:
            if not os.path.isdir(params['out_dir']):
                os.mkdir(params['out_dir'])

            img = cv2.imread(os.path.join(params['img_dir'], file + '.png'))
            if img is None:
                continue

            img = draw_boxes(img, gt_boxes, (255, 0, 0), 10)
            img = draw_boxes(img, pred_boxes, (0, 0, 255), 5)
            img = draw_boxes(img, page_bboxes.loc[pred_labels > -1, ['x0', 'y0', 'x1', 'y1']].to_numpy(), (0, 0, 255), 2)
            cv2.imwrite(os.path.join(params['out_dir'], file + '_result.png'), img)

    precision_iou = np.mean(pred_iou)
    recall_iou = np.mean(gt_iou)

    tpos = sum(tpos)
    fpos = sum(fpos)
    fneg = sum(fneg)
    if tpos + fpos == 0:
        precision = 0.
    else:
        precision = tpos / (tpos + fpos)
    recall = tpos / (tpos + fneg)

    print(f'node precision:                {precision: .4}')
    print(f'node recall:                   {recall: .4}')
    print()
    print(f'IoU precision:                 {precision_iou: .4}')
    print(f'IoU recall:                    {recall_iou: .4}')
    print()


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='evaluate model')
    argc.add_argument('params', type=str, help='evaluation params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    run_eval(params)
