from inference.entity_detector.entity_detector import GraphEntityDetector
from inference.inference_utils import bounding_box, draw_boxes
from siamese import models
from siamese.data import load_data_set, create_data_list
from sklearn.cluster import DBSCAN
from ops.bbox import batch_iou
from torch_geometric.nn import to_hetero
from tqdm import tqdm
import torch
import os
import numpy as np
import yaml
import argparse
import pickle
import cv2

def run_eval(params):
    nodes, edges, bboxes, feature_categories = load_data_set(
        os.path.join(params['data_dir'], params['data_tag']), 'test', categories=True)
    data_list, file_list, metadata, _ = create_data_list(
        nodes, edges, bboxes, feature_categories, params['text_embedding_model_path'], params['device'], params['target_name'])

    for feat_type, dropout_p in params['dropout'].items():
        feature_categories.loc[feature_categories.feature_type == feat_type, 'dropout'] = dropout_p

    model_class = eval(f"models.{params['model']['model_class']}")
    embedding_model_checkpoint = torch.load(os.path.join(params['model_dir'], params['model']['checkpoint']))
    embedding_model = model_class(params['model']['hidden_size'], params['model']['node_embedding_size'],
                                  params['model']['text_embedding_size'], feature_categories)

    embedding_model.gcnn = to_hetero(embedding_model.gcnn, metadata, aggr='sum')
    embedding_model.load_state_dict(embedding_model_checkpoint['model_state_dict'])

    if 'cluster_classifier' in embedding_model_checkpoint:
        cluster_classifier = pickle.loads(embedding_model_checkpoint['cluster_classifier'])
    else:
        cluster_classifier = None

    clustering_model = DBSCAN(eps=params['model']['cluster_eps'], min_samples=2)

    model_package = {
        'embedding_model_state_dict': embedding_model.state_dict(),
        'clustering_model': clustering_model,
        'cluster_classifier': cluster_classifier,
        'metadata': metadata
    }

    torch.save(model_package, os.path.join(params['model_dir'], 'model.pt'))


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='package model for deployment')
    argc.add_argument('params', type=str, help='evaluation params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    run_eval(params)

    # TODO: this script, better