from torch_geometric.data import HeteroData
from siamese.models.mutlilingual import MultilingualEmbedding
import torch
import pandas as pd
import numpy as np
import os


def create_data_list(nodes, edges, bboxes, feature_categories, embedding_model_path, device, label_name='label'):
    """
        Create torch dataset
    """
    #edges['edge_type'] = edges.edge_type.apply(lambda x: x.split('.')[1])
    #edges = edges[edges.edge_type != 'SEGMENT']
    edge_types = [('node', e, 'node') for e in edges.edge_type.unique()]
    metadata = (['node'], edge_types)

    if embedding_model_path is not None:
        ml_embedding_model = MultilingualEmbedding(embedding_model_path)

    text_col_names = feature_categories.col_name[feature_categories.feature_type == 'text'].tolist()
    drop_cols = ['file'] + text_col_names + [col for col in nodes.columns if col.endswith('_id')]
    feature_categories = feature_categories[feature_categories.feature_type != 'text']

    data_list = []
    file_list = nodes['file'].unique().tolist()
    for file_name in file_list:

        file_data = HeteroData()

        # node features
        file_node_features = torch.tensor(
            nodes[nodes.file == file_name].drop(columns=drop_cols, errors='ignore').values.astype(np.float32))

        if len(text_col_names) and 'text_embedding_0' not in nodes:
            text_list = nodes[nodes.file == file_name][text_col_names[0]].tolist()
            file_node_features = ml_embedding_model.forward(file_node_features, text_list)

        file_data['node'].x = file_node_features

        # node bboxes
        file_node_bboxes = torch.tensor(
            bboxes[bboxes.file == file_name].drop(columns=drop_cols, errors='ignore').values.astype(np.float32))
        file_data['node'].bbox = file_node_bboxes

        # node labels
        file_labels = torch.tensor(
            nodes[nodes.file == file_name][label_name].values.astype(np.float32))
        file_data['node'].y = file_labels

        # edges
        file_edges = edges[edges.file == file_name].drop(columns=['file'])
        for edge_type in metadata[1]:
            file_data[edge_type].edge_index = torch.tensor(
                file_edges.loc[file_edges.edge_type == edge_type[1], ['source', 'target']].values.astype(int).T)

            edge_attr = torch.tensor(
                file_edges.loc[file_edges.edge_type == edge_type[1],
                               [i for i in file_edges.columns if i not in ['source', 'target', 'edge_type']]
                               ].values.astype(np.float32))

            file_data[edge_type].edge_attr = edge_attr

        data_list.append(file_data.to(device))

    return data_list, file_list, metadata, feature_categories


def load_data_set(data_dir, split='', categories=False):
    nodes = pd.read_csv(os.path.join(data_dir, f'nodes.csv'), index_col=0)
    edges = pd.read_csv(os.path.join(data_dir, f'edges.csv'), index_col=0)
    bboxes = pd.read_csv(os.path.join(data_dir, f'bboxes.csv'), index_col=0)

    if len(split):
        splits = pd.read_csv(os.path.join(data_dir, f'splits.csv'))
        nodes = nodes[nodes.file.isin(splits.loc[splits.split == split, 'file'])]
        edges = edges[edges.file.isin(splits.loc[splits.split == split, 'file'])]
        bboxes = bboxes[bboxes.file.isin(splits.loc[splits.split == split, 'file'])]
    out = [nodes, edges, bboxes]

    if categories:
        cats = pd.read_csv(os.path.join(data_dir, f'feature_categories.csv'), index_col=0)
        out.append(cats)

    return tuple(out)
