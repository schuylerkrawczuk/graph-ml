from siamese.data import create_data_list, load_data_set
from siamese.models.gcnn import GCNN
from sklearn.cluster import DBSCAN
from inference.entity_detector.table_detector import GraphTableDetector
from inference.inference_utils import bbox_from_labels
from torch_geometric.nn import to_hetero
import torch
import numpy as np
import pandas as pd
import cv2
import json
import os
import argparse
import yaml
from tqdm import tqdm


BBOX_COLS = ['x0', 'y0', 'x1', 'y1']


def get_line_level_bboxes(table_detector, header_graph, column_graph, bboxes):

    segment_label_df = table_detector.line_level_detection(column_graph, header_graph, bboxes)

    # node-level cluster bboxes
    header_boxes = bbox_from_labels(segment_label_df.header, bboxes)
    column_boxes = bbox_from_labels(segment_label_df.column, bboxes)
    table_bboxes = bbox_from_labels(segment_label_df.table, bboxes)

    return header_boxes, column_boxes, table_bboxes


def main(params):
    annotations = json.load(open(params['annotation_path']))
    annotation_images = pd.DataFrame(annotations['images'])

    # header data
    header_nodes, header_edges, segment_bboxes = load_data_set(
        f'{params["root_path"]}/{params["header_model"]["data_tag"]}/', 'train', bbox=True)
    header_data_list, header_file_list, header_metadata = create_data_list(header_nodes, header_edges, 'header_id')

    col_nodes, col_edges = load_data_set(
        f'{params["root_path"]}/{params["column_model"]["data_tag"]}/', 'train', bbox=False)
    col_data_list, col_file_list, col_metadata = create_data_list(col_nodes, col_edges, 'label')
    col_file_map = [np.where(i == np.array(col_file_list))[0][0] for i in header_file_list]

    # header model
    header_embedding_model = GNN(
        params['header_model']['n_channels'],
        params['header_model']['embedding_size'],
        params['header_model']['normalized'])
    header_embedding_model = to_hetero(header_embedding_model, header_metadata, aggr='sum')
    header_embedding_model.load_state_dict(
        torch.load(params['header_model']['checkpoint'])['model_state_dict'])
    header_clustering_model = DBSCAN(eps=params['header_model']['cluster_eps'], min_samples=2)

    # col model
    col_embedding_model = header_embedding_model = GNN(
        params['column_model']['n_channels'],
        params['column_model']['embedding_size'],
        params['column_model']['normalized'])
    col_embedding_model = to_hetero(col_embedding_model, col_metadata, aggr='sum')
    col_embedding_model.load_state_dict(
        torch.load(params['column_model']['checkpoint'])['model_state_dict'])
    col_clustering_model = DBSCAN(eps=params['column_model']['cluster_eps'], min_samples=2)

    table_detector = GraphTableDetector(col_embedding_model, header_embedding_model,
                                        col_clustering_model, header_clustering_model)

    save_boxes = pd.DataFrame()
    for i, (header_graph, file) in tqdm(enumerate(zip(header_data_list, header_file_list))):
        column_graph = col_data_list[col_file_map[i]]
        image_id = annotation_images[annotation_images.file_name == file + '.png'].id.iloc[0]
        img = cv2.imread(params['img_dir'] + file + '.png')

        # all segment bboxes for file
        bboxes = segment_bboxes.loc[segment_bboxes.file == file, BBOX_COLS].values
        bboxes[:, ::2] *= img.shape[1]
        bboxes[:, 1::2] *= img.shape[0]
        bboxes = bboxes.astype(int)

        header_boxes, column_boxes, table_bboxes = get_line_level_bboxes(
            table_detector, header_graph, column_graph, bboxes)

        save_boxes = save_boxes.append(
            pd.DataFrame({
                'file': len(table_bboxes) * [file],
                'image_id': len(table_bboxes) * [image_id],
                'boxes': table_bboxes.tolist()
            }), ignore_index=True)

    save_boxes.to_csv(os.path.join(params['out_dir'], 'table_det_bboxes.csv'))


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='table detection')
    argc.add_argument('params', type=str, help='table model params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())

    main(params)

