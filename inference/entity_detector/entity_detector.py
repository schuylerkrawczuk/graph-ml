from ops.graph import separate_connected_components
from ops.bbox import batch_overlap
from sklearn.cluster import DBSCAN
from torch_geometric.data import HeteroData
from torch import nn
import torch
import numpy as np


class GraphEntityDetector:
    def __init__(self, embedding_model: nn.Module, clustering_model: DBSCAN, cluster_classifier=None):
        self.embedding_model = embedding_model
        self.embedding_model.eval()
        self.clustering_model = clustering_model
        self.cluster_classifier = cluster_classifier

    def get_node_labels(self, graph: HeteroData):
        node_embeddings = self.embedding_model(graph.x_dict, graph.edge_index_dict)
        node_embeddings = node_embeddings.detach()
        cluster_labels = self.clustering_model.fit_predict(node_embeddings)
        cluster_labels = self.split_unconnected_clusters(graph, cluster_labels)
        if self.cluster_classifier is not None:
            cluster_labels = self.remove_false_clusters_classifier(node_embeddings, cluster_labels)
            cluster_labels_row = self.join_clusters_with_row(graph, cluster_labels)
            cluster_labels = self.remove_false_clusters_classifier(node_embeddings, cluster_labels_row)

        return cluster_labels

    @staticmethod
    def split_unconnected_clusters(graph, cluster_labels):
        for cluster_label in np.unique(cluster_labels[cluster_labels > -1]):
            cluster_inds = np.where(cluster_labels == cluster_label)[0]
            split_sublabels = separate_connected_components(graph, cluster_inds)

            if len(np.unique(split_sublabels)) > 1:
                for sublabel in np.unique(split_sublabels):
                    sublabel_inds = split_sublabels == sublabel
                    if sum(sublabel_inds) > 1:
                        new_label = cluster_labels.max() + 1
                    else:
                        new_label = -1

                    cluster_labels[cluster_inds[sublabel_inds]] = new_label

        return cluster_labels

    @staticmethod
    def join_clusters_with_row(graph, cluster_labels):
        for cluster_label in np.unique(cluster_labels[cluster_labels > -1]):
            if not (cluster_labels == cluster_label).any():
                continue
            graph_bbox = graph.bbox_dict['node'].numpy()
            label_bbox = np.array([[
                graph_bbox[cluster_labels == cluster_label][:, 0].min(),
                graph_bbox[cluster_labels == cluster_label][:, 1].min(),
                graph_bbox[cluster_labels == cluster_label][:, 2].max(),
                graph_bbox[cluster_labels == cluster_label][:, 3].max()
            ]])
            y_overlap = batch_overlap(graph_bbox, label_bbox, dim=1)[:, 0]
            cluster_labels[y_overlap >= 0.3] = cluster_label
        return cluster_labels

    def remove_false_clusters_classifier(self, node_embeddings, cluster_labels):
        for cluster_label in np.unique(cluster_labels[cluster_labels > -1]):
            cluster_embedding = node_embeddings[cluster_labels == cluster_label].mean(dim=0, keepdim=True)
            if not self.cluster_classifier.predict(cluster_embedding):
                cluster_labels[cluster_labels == cluster_label] = -1
        return cluster_labels

    @staticmethod
    def remove_false_clusters(node_classes, cluster_labels):
        for cluster_label in np.unique(cluster_labels[cluster_labels > -1]):
            cluster_node_labels = node_classes[cluster_labels == cluster_label]
            if cluster_node_labels.mean() < 0.5:
                cluster_labels[cluster_labels == cluster_label] = -1
        return cluster_labels