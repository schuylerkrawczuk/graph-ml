from inference.entity_detector.entity_detector import GraphEntityDetector
from inference.inference_utils import bounding_box, get_tokens_within_bbox, get_tokens_intersecting_bbox, dimension_overlap
from ops.bbox import batch_iou
import pandas as pd
import numpy as np


class GraphTableDetector:
    def __init__(self, col_embedding_model, header_embedding_model,
                 col_clustering_model, header_clustering_model):

        self.header_detector = GraphEntityDetector(header_embedding_model, header_clustering_model)
        self.column_detector = GraphEntityDetector(col_embedding_model, col_clustering_model)

    def line_level_detection(self, column_graph, header_graph, segment_bboxes):
        header_labels = self.header_detector.get_node_labels(header_graph)
        column_labels = self.column_detector.get_node_labels(column_graph)

        # removing non-relevant header clusters
        header_node_features = header_graph.x_dict['node']
        for hl in header_labels:
            if header_node_features[header_labels == hl, -1].mean() < 0.5:
                header_labels[header_labels == hl] = -1

        # get table labels, create dataframe of line level entity labels
        segment_label_df = self.table_detection(segment_bboxes, column_labels, header_labels)

        return segment_label_df

    def table_detection(self, bboxes, column_labels, header_labels):
        # Header/column combination heuristics
        segment_label_df = pd.DataFrame({
            'header': header_labels,
            'column': column_labels,
            'table': -1
        })
        bbox_df = pd.DataFrame(bboxes.tolist(), columns=['x0', 'y0', 'x1', 'y1'])
        segment_label_df = segment_label_df.join(bbox_df)

        segment_label_df = self.header_postprocess(segment_label_df)
        segment_label_df = self.trim_columns_with_header(segment_label_df)
        segment_label_df = self.attach_columns_to_headers(segment_label_df)

        # Table grouping heuristics
        # group header columns as tables
        column_header_mask = np.logical_and(segment_label_df.header > -1, segment_label_df.column > -1)
        column_headers = segment_label_df[column_header_mask]

        # get segments for columns corresponding to header, label as table
        for header_id, header_segments in column_headers.groupby('header'):
            segment_label_df.loc[segment_label_df.column.isin(header_segments.column), 'table'] = header_id

        return segment_label_df

    def header_postprocess(self, segment_label_df):
        """
        combine overlapping headers, remove invalid headers
        """
        header_bboxes, header_labels = self.get_header_bboxes(segment_label_df)

        # label all segments within header bbox as header
        for header_bbox, header_label in zip(header_bboxes, header_labels):
            segment_label_df.loc[
                get_tokens_within_bbox(segment_label_df, header_bbox).index, 'header'] = header_label

        # merging overlapping header clusters
        if len(header_bboxes):
            intersections = np.triu(batch_iou(header_bboxes, header_bboxes), k=1)
            merge_inds = np.where(intersections > 0)

            for merge_x, merge_y in zip(*merge_inds):
                segment_label_df.loc[
                    segment_label_df.header == header_labels[merge_y], 'header'] = header_labels[merge_x]

            header_bboxes, header_labels = self.get_header_bboxes(segment_label_df)

        # header validation: check if multi-line/single line, check it isn't column-like
        # check if column is multiline
        for header_label in header_labels:
            # strict check
            n_header_lines = len(segment_label_df[segment_label_df.header == header_label].groupby(['y0', 'y1']))
            segment_label_df.loc[segment_label_df.header == header_label, 'multiline_header'] = n_header_lines > 1

        # check to remove column-like header pieces TODO
        # break/de-assign columns than span across a header without being in cluster TODO

        return segment_label_df

    def trim_columns_with_header(self, segment_label_df):
        """
        Remove parts of columns that are above the header
        """
        header_bboxes, header_labels = self.get_header_bboxes(segment_label_df)
        column_header_mask = np.logical_and(segment_label_df.header > -1, segment_label_df.column > -1)
        column_headers = segment_label_df[column_header_mask]

        for _, col_header in column_headers.iterrows():
            column_bbox = bounding_box(segment_label_df[segment_label_df.column == col_header['column']])
            header_bbox = header_bboxes[col_header['header'] == header_labels][0]

            if column_bbox[1] < header_bbox[1]:
                y0_cutoff = header_bbox[1]

                new_column_bbox = np.array([column_bbox[0], column_bbox[1], column_bbox[2], y0_cutoff])
                column_bbox[1] = y0_cutoff
                new_col_label = segment_label_df.column.max() + 1
                new_col_bbox_df = get_tokens_within_bbox(segment_label_df, new_column_bbox)
                segment_label_df.loc[new_col_bbox_df.index, 'column'] = new_col_label

                # if header is only segment in column, unassign from column
                if (segment_label_df.column == col_header['column']).sum() == 1:
                    segment_label_df.loc[segment_label_df.column == col_header['column'], 'column'] = -1

        return segment_label_df

    def attach_columns_to_headers(self, segment_label_df):
        """
        find or create candidate column to attach to a header without a column
        TODO: refactor, clean up.
        """
        column_header_mask = np.logical_and(segment_label_df.header > -1, segment_label_df.column > -1)
        column_headers = segment_label_df[column_header_mask]

        for header_label in segment_label_df.header:
            header_segments = segment_label_df[segment_label_df.header == header_label]
            header_segments_without_columns = header_segments[
                np.logical_and(
                    header_segments.header == header_label,
                    header_segments.column == -1
                )]

            # if header segment doesn't have column, attach one of the length of the longest column
            attached_columns = column_headers[column_headers.header == header_label].column
            if len(header_segments_without_columns) and len(attached_columns):

                column_boxes, column_names = self.get_column_boxes(segment_label_df)

                attached_column_boxes = column_boxes[[np.argmax(column_names == c) for c in attached_columns]]
                max_y1 = attached_column_boxes[:, 3].max()

                for _, header_segment in header_segments_without_columns.iterrows():
                    new_column_index = segment_label_df.column.max() + 1
                    new_column_bbox = [header_segment['x0'] - 75, header_segment['y0'],
                                       header_segment['x1'] + 75, max_y1]
                    # TODO: do this using graph edges instead of distance ^

                    # segments within proposed column
                    new_col_segments = get_tokens_intersecting_bbox(segment_label_df, new_column_bbox)

                    # unlabeled, non-header segments in proposed column
                    col_proposal_segments = np.logical_and(
                        new_col_segments.column == -1,
                        new_col_segments.header == -1
                    )

                    if sum(col_proposal_segments):
                        # assign unlabeled segments and header to new proposed column
                        col_proposal_segments[header_segment.name] = True
                        segment_label_df.loc[
                            new_col_segments[col_proposal_segments].index, 'column'] = new_column_index
                    else:
                        # check for column within max_y1 of header, attach to it,
                        # check for column in proposal bbox w/o header

                        column_boxes, column_names = self.get_column_boxes(segment_label_df)

                        # columns in proposal bbox that don't have a header
                        unmatched_headerless_cols = new_col_segments[
                            np.logical_and(
                                ~new_col_segments.column.isin(column_headers.column.unique()),
                                new_col_segments.column > -1
                            )].column.unique()

                        # bboxes of candidate columns
                        candidate_col_boxes = column_boxes[
                            [np.argmax(column_names == c) for c in unmatched_headerless_cols]]

                        if len(candidate_col_boxes):
                            # of these, take column with closest x
                            col_match_ind = dimension_overlap(
                                header_segment[['x0', 'y0', 'x1', 'y1']].values[None, :],
                                candidate_col_boxes
                            ).argmax()

                            unmatched_headerless_col = unmatched_headerless_cols[col_match_ind]

                            # set header to be part of this column
                            segment_label_df.loc[header_segment.name, 'column'] = unmatched_headerless_col
                            header_segment['column'] = unmatched_headerless_col
                            column_headers.loc[header_segment.name] = header_segment

        return segment_label_df

    @staticmethod
    def get_header_bboxes(segment_label_df):
        header_labels = segment_label_df[segment_label_df.header > -1].header.unique()
        header_bboxes = []
        for header_id in header_labels:
            header_bboxes.append(bounding_box(segment_label_df[segment_label_df.header == header_id]))
        return np.array(header_bboxes), header_labels

    @staticmethod
    def get_column_boxes(segment_label_df):
        col_boxes = []
        col_inds = []
        for column in sorted(segment_label_df[segment_label_df.column > -1].column.unique()):
            col_boxes.append(bounding_box(segment_label_df[segment_label_df.column == column]))
            col_inds.append(column)
        return np.array(col_boxes), np.array(col_inds)
