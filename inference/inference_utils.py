import numpy as np
import pandas as pd
import cv2


def intersection(bb1, bb2):
    """Get intersection of two boxes"""
    box_x0 = max(bb1[0], bb2[0])
    box_y0 = max(bb1[1], bb2[1])
    box_x1 = min(bb1[2], bb2[2])
    box_y1 = min(bb1[3], bb2[3])
    if box_x1 - box_x0 < 0 or box_y1 - box_y0 < 0:
        return None
    return box_x0, box_y0, box_x1, box_y1


def bounding_box(data_frame) -> np.array:
    """get bounding box of all elements in DataFrame or array"""
    if isinstance(data_frame, np.ndarray):
        data_frame = pd.DataFrame(data_frame, columns=['x0', 'y0', 'x1', 'y1'])
    return data_frame.agg({
        'x0': min,
        'y0': min,
        'x1': max,
        'y1': max}).to_numpy()


def dimension_overlap(bbox1:np.ndarray, bbox2:np.ndarray, dim=0):
    """
    Overlap of two sets of bboxes in one dimension. 0: x, 1: y
    """
    x0 = np.maximum(bbox1[:, dim], bbox2[:, dim])
    x1 = np.minimum(bbox1[:, dim + 2], bbox2[:, dim + 2])
    return x1 - x0


def get_tokens_within_bbox(text_df, bbox):
    """
    Return sub-set of dataframe that is contained within bounding box
    """
    bbox_location = (text_df.x0 >= bbox[0] - 1) & \
                    (text_df.x1 <= bbox[2] + 1) & \
                    (text_df.y0 >= bbox[1] - 1) & \
                    (text_df.y1 <= bbox[3] + 1)

    return text_df[bbox_location]


def get_tokens_intersecting_bbox(text_df, bbox):
    """
    Return sub-set of dataframe that is contained within bounding box
    """
    bbox_location = text_df.apply(
        lambda x: intersection([x['x0'], x['y0'], x['x1'], x['y1']], bbox) is not None, axis=1
    )

    return text_df[bbox_location]


def bbox_from_labels(labels, bboxes):
    containing_boxes = []
    for cl in np.unique(labels[labels > -1]):
        cluster_boxes = bboxes[labels == cl]
        cluster_containing_box = np.hstack([cluster_boxes.min(axis=0)[:2], cluster_boxes.max(axis=0)[2:]])
        containing_boxes.append(cluster_containing_box)
    return np.array(containing_boxes)


def draw_boxes(img, boxes, color, lw):
    if len(boxes):
        boxes[:, ::2] *= img.shape[1]
        boxes[:, 1::2] *= img.shape[0]
        boxes = boxes.astype(int)
        for box in boxes:
            img = cv2.rectangle(img, (box[0], box[1]), (box[2], box[3]), color, lw)
    return img
