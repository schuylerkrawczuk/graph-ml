import numpy as np
import pandas as pd


def get_connected_nodes(nodes: list, edges: pd.DataFrame):
    """
    Get nodes directly connected to subgraph
    """
    connected_nodes = np.append(
        edges.loc[edges.source.isin(nodes), 'target'].values,
        edges.loc[edges.target.isin(nodes), 'source'].values
    )

    return np.setdiff1d(connected_nodes, nodes).astype(int)


def get_subgraph_edges(p, edges):
    """
    Get the edges connecting a set of nodes
    """
    p_edge_mask = np.logical_and(edges.target.isin(p), edges.source.isin(p))
    return edges[p_edge_mask]


def get_edges_connecting(subgraph, node, edges):
    """
    get edge types connecting a node to a subgraph
    """
    connecting_edge_mask = np.logical_or(
        np.logical_and(edges.source.isin(subgraph), edges.target.isin(node)),
        np.logical_and(edges.target.isin(subgraph), edges.source.isin(node)))

    return edges[connecting_edge_mask].edge_type.unique()
