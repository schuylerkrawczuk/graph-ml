"""
    Modified implementation of Gapprox frequent subgraph mining algorithm with simplification for our use case
"""
from data_creation.subgraph_mining.subgraph_utils import get_connected_nodes
from itertools import combinations
import numpy as np
import pandas as pd


class GApprox:
    def __init__(self, nodes: np.ndarray, edges: pd.DataFrame, similarity_tol: float, support_threshold: int):
        self.nodes_all = nodes
        self.edges_all = edges
        self.nodes = nodes
        self.edges = edges

        self.similarity_tol = similarity_tol
        self.support_threshold = support_threshold

        self.node_distances = np.linalg.norm(self.nodes - self.nodes[:, None, :], axis=2)
        self.interchangable_nodes = self.node_distances <= similarity_tol

        self.marked = np.zeros(len(nodes), dtype=bool)
        self.to_unmark = []

        self.patterns = []
        self.occurrences = []

    def get_equivalents(self, node_idx):
        """
        Get node feature equivalents for node at index node_idx
        """
        same_node_types = np.argwhere(self.interchangable_nodes[node_idx])
        return same_node_types.tolist()

    def explore(self):
        if len(self.edges):
            node_idx = int(self.edges[['source', 'target']].values.flatten().min())
            self.marked[node_idx] = True
            m_p = self.get_equivalents(node_idx)
            self.marked[[i[0] for i in m_p]] = True

            self.dfs_vertical([node_idx], m_p)
            self.remove_node(node_idx)

            self.explore()

    def dfs_vertical(self, p, m_p):
        """
        gives all sets of connected vertices consisting of P and unmarked vertices
        """
        v_expand = get_connected_nodes(p, self.edges)
        v_expand = [v for v in v_expand if not self.marked[v]]
        self.marked[v_expand] = True
        self.dfs_horizontal(p, m_p, v_expand)
        self.marked[v_expand] = False

    def dfs_horizontal(self, p, m_p, v_expand):
        """
        returns all connected vertex sets consisting of p, a subset of v_expand, and unmarked vertices
        """

        for i, v_i in enumerate(v_expand):
            p_new = p + [v_i]
            m_p_new = []
            for i_mp in range(len(m_p)):
                equivalents = self.get_equivalents(v_i)

                # get m_p neighborhood
                occurrence_neighbors = get_connected_nodes(m_p[i_mp], self.edges_all)

                # get intersect of these two lists
                candidates = np.intersect1d(equivalents, occurrence_neighbors)
                candidates = candidates[~self.marked[candidates]]
                if len(candidates):
                    m_p_new.append(m_p[i_mp] + [candidates[0]])
                    self.marked[candidates[0]] = True
                    self.to_unmark.append(candidates[0])

            # check support
            sup = self.upper_bound(m_p_new)
            if sup < self.support_threshold:
                self.marked[self.to_unmark] = False
                self.to_unmark = []
                continue

            self.patterns.append(p_new)
            self.occurrences.append(m_p_new)

            v_expand_new = v_expand[i + 1:]
            self.dfs_horizontal(p_new, m_p_new, v_expand_new)
            self.marked[self.to_unmark] = False
            self.to_unmark = []
            self.dfs_vertical(p_new, m_p_new)

    @staticmethod
    def upper_bound(m_p):
        """
        Approximates upper bound for pattern support
        """
        support_bound = 0
        mp_array = np.atleast_2d(np.array(m_p))
        while all(mp_array.shape):
            support_bound += 1
            node_idxs, cts = np.unique(mp_array, return_counts=True)
            most_frequent = node_idxs[cts.argmax()]
            mp_array = mp_array[~np.any(mp_array == most_frequent, axis=1)]

        return support_bound

    def remove_node(self, node_idx):
        """
        Remove node from exploration space
        """
        self.nodes = self.nodes[node_idx + 1:]
        self.edges = self.edges[
            np.logical_and(
                self.edges.target != node_idx,
                self.edges.source != node_idx)
        ]

    def postprocess_occurrences(self):
        """
        group into sets of overlapping motifs. For a group of overlapping, take only the one with:
            - the most support
            - the most nodes
        All other patterns overlapping with this one are removed
        """

        if not len(self.patterns):
            return []

        # add pattern to its occurrence list if not present
        pattern_occurrences = []
        for pattern, occurrences in zip(self.patterns, self.occurrences):
            occurrences = sorted([sorted(o) for o in occurrences])
            pattern = sorted(pattern)
            if pattern not in occurrences:
                occurrences = [pattern] + occurrences
            pattern_occurrences.append(occurrences)

        self.occurrences = pattern_occurrences

        # take the longest pattern of those w/ same support + starting nodes
        max_size = max(list(map(lambda x: len(x[0]), self.occurrences)))

        subgraph_df = pd.DataFrame({'length': [], 'support': [], 'start': []})
        for i, subgraphs in enumerate(self.occurrences):
            subgraph_df = subgraph_df.append({
                'support': len(subgraphs),
                'length': len(subgraphs[0]),
                'start': ' '.join([str(s[0]) for s in sorted(subgraphs)])
            }, ignore_index=True)

        longest_inds = []
        for _, start_subgraphs in subgraph_df.groupby('start'):
            longest_subgraph = start_subgraphs.sort_values(by='length', ascending=False).iloc[0]
            if longest_subgraph.length == max_size:
                longest_inds.append(longest_subgraph.name)

        # if any have overlapping nodes with a longer one, discard
        longest_subgraphs = np.array(self.occurrences)[longest_inds].tolist()

        new_longest_subgraphs = []
        new_longest = 0
        if len(longest_subgraphs) > 1:
            for sg1, sg2 in combinations(longest_subgraphs, 2):
                if len(np.intersect1d(sg1, sg2)):
                    if len(sg1) < len(sg2):
                        if sg2 not in new_longest_subgraphs and len(sg2) > new_longest:
                            new_longest_subgraphs.append(sg2)
                            new_longest = len(sg2)
                    else:
                        if sg1 not in new_longest_subgraphs and len(sg1) > new_longest:
                            new_longest_subgraphs.append(sg1)
                            new_longest = len(sg1)

            longest_subgraphs = new_longest_subgraphs

        return longest_subgraphs


