from bliss.util.table_util.table_detection_utils import bounding_box, bounding_box_normalized
from bliss.invoice.extraction.features.text_line_feature_generation import GenerateTextLineFeatures
from ops.graph import graph_from_df, connected_neighbors
import numpy as np
import pandas as pd


def get_percent_numeric(text_df, node_level, edges, node_bboxes):
    percent_numeric = text_df.groupby(node_level).apply(
        lambda x: ''.join(x.text)).apply(lambda x:
                                         np.mean([1 if i.isnumeric() else 0 for i in x]))
    return percent_numeric.to_frame().rename({0: 'percent_numeric'}, axis=1)


def get_bounding_box(text_df, node_level, edges, node_bboxes):
    node_bbox = text_df.groupby(node_level).apply(bounding_box_normalized)
    node_bbox = node_bbox.to_frame().rename({0: 'bbox'}, axis=1)
    return pd.DataFrame(node_bbox.bbox.tolist(), columns=['x0', 'y0', 'x1', 'y1'],
                        index=node_bbox.index)


def get_text(text_df, node_level, edges, node_bboxes):
    text_series = text_df.groupby(node_level).text.apply(lambda x: ' '.join(x))
    return text_series.to_frame().rename({0: 'text'}, axis=1)


def get_text_height(text_df, node_level, edges, node_bboxes):
    text_df['height'] = text_df.y1 - text_df.y0
    height = text_df.groupby(node_level).height.agg(np.median)
    height /= height.max()
    return height.to_frame().rename({0: 'text_height'}, axis=1)


def get_char_width(text_df, node_level, edges, node_bboxes):
    width = text_df.groupby(node_level).char_width.agg(np.median)
    return width.to_frame().rename({0: 'char_width'}, axis=1)


def get_line_adjacency(text_df, node_level, edges, node_bboxes):
    line_cols = ['line_above', 'line_below']  # , 'line_left', 'line_right']
    if not np.isin(line_cols, text_df.columns).all():
        for col in line_cols:
            text_df[col] = -1
    line_adj = text_df.groupby(node_level)[line_cols].agg(lambda x: any(x > -1)).astype(int)
    return line_adj


def get_line_structural_features(text_df, node_level, edges, node_bboxes):
    dims = {'y': np.round(np.median(text_df.y0 / text_df.y0_norm)),
            'x': np.round(np.median(text_df.x0 / text_df.x0_norm))}
    feature_gen = GenerateTextLineFeatures(text_df, dims)
    line_feature_df = feature_gen.get_text_line_df_with_features()
    line_feature_df = line_feature_df.drop('uniline_text', axis=1)
    line_columns = [i for i in line_feature_df if i.startswith('uniline')]
    text_df = pd.merge(text_df, line_feature_df[['line_number'] + line_columns], on='line_number', how='left')
    structural_features = text_df.groupby(node_level)[line_columns].agg(max)
    return structural_features


def get_closest_node_distances(text_df, node_level, edges, node_bboxes):
    graph = graph_from_df(edges)
    for segment_number, segment_df in text_df.groupby(node_level):
        neighbors = connected_neighbors(graph, segment_number, max_depth=1)
        segment_bbox = node_bboxes.loc[segment_number]
        neighbor_bboxes = node_bboxes.loc[neighbors]

        upward_distance = neighbor_bboxes.y1 - segment_bbox.y0
        text_df.loc[text_df[node_level] == segment_number, 'min_upward_dist'] = upward_distance[upward_distance > 0].min()

        downward_distance = neighbor_bboxes.y0 - segment_bbox.y1
        text_df.loc[text_df[node_level] == segment_number, 'min_downward_dist'] = downward_distance[downward_distance > 0].min()

        right_distance = neighbor_bboxes.x0 - segment_bbox.x1
        text_df.loc[text_df[node_level] == segment_number, 'min_right_dist'] = right_distance[right_distance > 0].min()

        left_distance = segment_bbox.x0 - neighbor_bboxes.x1
        text_df.loc[text_df[node_level] == segment_number, 'min_left_dist'] = left_distance[left_distance > 0].min()

        text_df.fillna(1., inplace=True)

        closest_node_features = text_df.groupby(node_level)[
            ['min_upward_dist', 'min_downward_dist', 'min_right_dist', 'min_left_dist']].agg(min)
        return closest_node_features


feature_functions = {
    'text': {
        'text': get_text
    },
    'text_features': {
        'percent_numeric': get_percent_numeric
    },
    'structural_features': {
        'bounding_box': get_bounding_box,
        # 'uniline_feats': get_line_structural_features,
        # 'closest_nodes': get_closest_node_distances

    },

    'line_features': {
        'line_adjacency': get_line_adjacency,
    }
}