from features import get_bounding_box, feature_functions
from bliss.invoice.extraction.graph_generation.page_graph import PageGraph
from bliss.invoice.extraction.graph_generation.entity_graph import EntityGraph
from tqdm import tqdm
import pandas as pd
import os
import argparse

VERTICAL_EDGES = ['RIGHT_ALIGNED', 'LEFT_ALIGNED', 'CENTER_ALIGNED', 'HORIZONTAL_OVERLAP']


def get_tokens_within_bbox(text_df, bbox):
    """
    Return sub-set of dataframe that is contained within bounding box
    """
    bbox_location = (text_df.x0 >= bbox[0] - 1) & \
                    (text_df.x1 <= bbox[2] + 1) & \
                    (text_df.y0 >= bbox[1] - 1) & \
                    (text_df.y1 <= bbox[3] + 1)

    return text_df[bbox_location]


def main(annotation_path, ocr_dir, target_name,
         node_level, feature_functions):
    annotation = pd.read_csv(annotation_path, index_col=0)
    for file_name in annotation.file.unique():
        annotation.loc[annotation.file == file_name, target_name] = annotation[
            annotation.file == file_name].reset_index().index

    all_nodes, all_edges, all_bboxes = [], [], []
    for file_name in tqdm(annotation.file.unique()):
        file_annotation = annotation[annotation.file == file_name]
        invoice_df = pd.read_csv(f'{os.path.join(ocr_dir, file_name)}.csv')
        invoice_df[target_name] = -1

        # generate segment graph
        try:
            invoice_df.dropna(subset=['text'], inplace=True)
            if 'height' not in invoice_df:
                invoice_df['height'] = invoice_df['y1'].astype(float) - invoice_df['y0'].astype(float)

            if 'char_width' not in invoice_df:
                invoice_df['char_width'] = (invoice_df['x1'].astype(float) - invoice_df['x0'].astype(float)) / \
                                            invoice_df['text'].apply(len)

        except:
            print(file_name)
            continue

        invoice_df, edges = PageGraph.build_page_level_graph(invoice_df)

        edges.dropna(inplace=True)

        # create up/down vertical edges
        edges['edge_type'] = edges.edge_type.apply(lambda x: x.name)
        edges = edges.append(
            edges[~edges.edge_type.str.startswith('NEAREST_NEIGHBOR')].rename({'source': 'target', 'target': 'source'},
                                                                              axis=1))
        vertical_edges = edges[edges.edge_type.isin(VERTICAL_EDGES)]
        vertical_edges.loc[vertical_edges.source > vertical_edges.target, 'edge_type'] += '_DOWN'  # TODO: this is backwards
        vertical_edges.loc[vertical_edges.source < vertical_edges.target, 'edge_type'] += '_UP'
        edges.loc[edges.edge_type.isin(VERTICAL_EDGES), 'edge_type'] = vertical_edges.edge_type

        # generate node features
        node_bbox = get_bounding_box(invoice_df, node_level, None, None)

        feature_sets = {}
        feature_categories = {}
        for feature_type in feature_functions:
            for feature_name, feature_function in feature_functions[feature_type].items():
                feature_sets[feature_name] = feature_function(invoice_df, node_level, edges, node_bbox)
                for col_name in feature_sets[feature_name].columns:
                    feature_categories[col_name] = feature_type

        node_features = pd.concat(feature_sets.values(), axis=1)

        # generate labels from annotation
        for _, box in file_annotation.iterrows():
            target_df = get_tokens_within_bbox(invoice_df,
                                               [box.x0, box.y0, box.x1, box.y1])

            invoice_df.loc[target_df.index, target_name] = box[target_name]
        labels = invoice_df.groupby(node_level).apply(
            lambda x: x[target_name].value_counts().keys()[0]).to_frame()
        labels.rename({0: target_name}, axis=1, inplace=True)

        node_features = pd.concat([node_features, labels], axis=1)
        node_features['file'] = file_name
        edges['file'] = file_name
        node_bbox['file'] = file_name

        all_nodes.append(node_features)
        all_edges.append(edges)
        all_bboxes.append(node_bbox)

    # feature categories
    feature_categories = {}
    for feature_type in feature_functions:
        for feature_name in feature_functions[feature_type]:
            for col_name in feature_sets[feature_name].columns:
                feature_categories[col_name] = feature_type
    feature_categories = pd.Series(feature_categories).to_frame().reset_index().rename(
        {'index': 'col_name', 0: 'feature_type'}, axis=1)

    all_nodes = pd.concat(all_nodes)
    all_edges = pd.concat(all_edges)
    all_bboxes = pd.concat(all_bboxes)

    all_nodes = all_nodes.drop_duplicates()
    all_edges = all_edges.drop_duplicates()
    all_bboxes = all_bboxes.drop_duplicates()

    all_nodes.to_csv('/mnt/nodes.csv')
    all_edges.to_csv('/mnt/edges.csv')
    all_bboxes.to_csv('/mnt/bboxes.csv')
    feature_categories.to_csv('/mnt/feature_categories.csv')


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='generate graph data from ocr')
    argc.add_argument('annotation_path', type=str, help='ann csv path')
    argc.add_argument('ocr_dir', type=str, help='dir to text dfs')
    argc.add_argument('target_name', type=str, help='name of target field, i.e. header_id')
    argp = argc.parse_args()

    main(argp.annotation_path, argp.ocr_dir, target_name=argp.target_name,
         node_level='segment_number', feature_functions=feature_functions)
